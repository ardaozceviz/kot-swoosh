package com.aozceviz.swoosh.Controller

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Toast
import com.aozceviz.swoosh.Model.Player
import com.aozceviz.swoosh.R
import com.aozceviz.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_league.*

class LeagueActivity : BaseActivity() {

    var player = Player("", "")

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(EXTRA_PLAYER, player)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if(savedInstanceState != null) {
            player = savedInstanceState.getParcelable(EXTRA_PLAYER)
        }
    }

    fun onMensClicked(View: View) {
        womensLeagueButton.isChecked = false
        coEdLeagueButton.isChecked = false
        if (player.league == "mens") {
            player.league = ""
        } else {
            player.league = "mens"
        }
    }

    fun onWomensClicked(View: View) {
        mensLeagueButton.isChecked = false
        coEdLeagueButton.isChecked = false
        if (player.league == "womens") {
            player.league = ""
        } else {
            player.league = "womens"
        }
    }

    fun onCoEdClicked(View: View) {
        womensLeagueButton.isChecked = false
        mensLeagueButton.isChecked = false
        if (player.league == "co-ed") {
            player.league = ""
        } else {
            player.league = "co-ed"
        }
    }

    fun leagueNextClicked(view: View) {
        if (player.league != "") {
            val skillActivity = Intent(this, SkillActivity::class.java)
            skillActivity.putExtra(EXTRA_PLAYER, player)
            startActivity(skillActivity)
        } else {
            Toast.makeText(this, "Please select a league", Toast.LENGTH_SHORT).show()
        }
    }
}
